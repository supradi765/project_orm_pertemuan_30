<!DOCTYPE html>
<html lang="id">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Halaman Contact</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <style>
    .container {
      padding: 20px;
    }
  </style>
</head>
<body>

    <p> @if (session('error'))
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ session('error') }}
        </div>
        @endif
        @if (session('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ session('success') }}
            </div>
        @endif</p>
    <h1><a href="{{ route('contact.form-input') }}" class="text-dark">Tambah contact</a></h1>
    <div class="card">
        <div class="card-header">
            <div class="container">
                <table class="table table-bordered">
                  <thead class="thead-dark">
                    <tr>
                      <th scope="col">No.</th>
                      <th scope="col">Nama</th>
                      <th scope="col">Alamat Email</th>
                      <th scope="col">Nomor Telepon</th>
                      <th scope="col">edit data</th>
                    </tr>
                  </thead>
                  @php $no=1; @endphp
                  @foreach($listContact as $key)
                  <tbody>
                    <tr>
                      <th scope="row">{{ $no++ }}</th>
                      <td>{{ $key->name }}</td>
                      <td>{{ $key->email }}</td>
                      <td>{{ $key->number_phone }}</td>
                      <td>
                        <a href="{{ route('contact.form-edit', $key->id) }}">edit</a>
                        <a href="{{ route('contact.delete-data', $key->id) }}" onclick="return confirm('Apakah Anda Ingin Mehapus Data ini?');">hapus</a>
                      </td>
                    </tr>
                  </tbody>
                  @endforeach
                </table>
              </div>
        </div>
    </div>




  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>

<!DOCTYPE html>
<html lang="id">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Halaman Contact</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <style>
    .container {
      padding: 20px;
    }
  </style>
</head>
<body>
    <p>@if (session('error'))
        {{ session('error') }}
      @endif
      @if(session('sucses'))
        {{ session('sucses') }}
      @endif</p>
  <div class="container">
    <h1><a href="{{ route('alamat.form') }}" class="text-dark">Tambah alamat</a></h1>
    <br>

    <table class="table table-bordered">
        <tr>
          <th scope="col">No.</th>
          <th scope="col">email</th>
          <th scope="col">Alamat lengkap</th>
          <th scope="col">edit data</th>
        </tr>
      @php $no = 1; @endphp
      @foreach($dataAlamat as $row)
        <tr>
          <td scope="row">{{ $no++ }}</td>
          <td scope="row">{{ $row->email }}</td>
          <td scope="row">{{ $row->alamat }}</td>
          <td>
            <a href="{{ route('alamat.delete-data', $row->id) }}">hapus data</a>
          </td>
        </tr>
      @endforeach
    </table>
  </div>

  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</body>
</html>
